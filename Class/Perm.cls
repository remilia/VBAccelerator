VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MachineCode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private ay(16) As Integer
Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Function RegSetValue Lib "advapi32.dll" Alias "RegSetValueA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal dwType As Long, ByVal lpData As String, ByVal cbData As Long) As Long
Private Declare Function RegCreateKey Lib "advapi32.dll" Alias "RegCreateKeyA" (ByVal hKey As Long, ByVal lpSubKey As String, phkResult As Long) As Long
Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Const HKEY_LOCAL_MACHINE = &H80000002
Const REG_SZ = 1
Public Enum RegCodeTypeEnum
    RegCodeType_1_DiskIDNumber = 1
    RegCodeType_3_CpuIDNumber = 3
    RegCodeType_5_MacNumber = 5
    RegCodeType_4_DiskID_CPUIDNumber = 4
    RegCodeType_6_DiskID_MacNumber = 6
    RegCodeType_8_CpuID_MacNumber = 8
    RegCodeType_9_CpuID_MacNumber_DiskID = 9
End Enum

Public Enum RegCodeLong
    RegCodeLong_Len_8 = 8
    RegCodeLong_Len_16 = 16
    RegCodeLong_Len_32 = 32
    RegCodeLong_Len_64 = 64
    RegCodeLong_Len_128 = 128
End Enum
Dim l As New DMD5Hash
Private Const NCBASTAT = &H33
Private Const NCBNAMSZ = 16
Private Const HEAP_ZERO_MEMORY = &H8
Private Const HEAP_GENERATE_EXCEPTIONS = &H4
Private Const NCBRESET = &H32

Private Type NCB
    ncb_command As Byte                                                         'Integer
    ncb_retcode As Byte                                                         'Integer
    ncb_lsn As Byte                                                             'Integer
    ncb_num As Byte                                                             ' Integer
    ncb_buffer As Long                                                          'String
    ncb_length As Integer
    ncb_callname As String * NCBNAMSZ
    ncb_name As String * NCBNAMSZ
    ncb_rto As Byte                                                             'Integer
    ncb_sto As Byte                                                             ' Integer
    ncb_post As Long
    ncb_lana_num As Byte                                                        'Integer
    ncb_cmd_cplt As Byte                                                        'Integer
    ncb_reserve(9) As Byte                                                      ' Reserved, must be 0
    ncb_event As Long
End Type


Private Type ADAPTER_STATUS
    adapter_address(5) As Byte                                                  'As String * 6
    rev_major As Byte                                                           'Integer
    reserved0 As Byte                                                           'Integer
    adapter_type As Byte                                                        'Integer
    rev_minor As Byte                                                           'Integer
    duration As Integer
    frmr_recv As Integer
    frmr_xmit As Integer
    iframe_recv_err As Integer
    xmit_aborts As Integer
    xmit_success As Long
    recv_success As Long
    iframe_xmit_err As Integer
    recv_buff_unavail As Integer
    t1_timeouts As Integer
    ti_timeouts As Integer
    Reserved1 As Long
    free_ncbs As Integer
    max_cfg_ncbs As Integer
    max_ncbs As Integer
    xmit_buf_unavail As Integer
    max_dgram_size As Integer
    pending_sess As Integer
    max_cfg_sess As Integer
    max_sess As Integer
    max_sess_pkt_size As Integer
    name_count As Integer
End Type


Private Type NAME_BUFFER
    Name As String * NCBNAMSZ
    name_num As Integer
    name_flags As Integer
End Type


Private Type ASTAT
    adapt As ADAPTER_STATUS
    NameBuff(30) As NAME_BUFFER
End Type

Private Declare Function Netbios Lib "netapi32.dll" (pncb As NCB) As Byte
Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" _
    (hpvDest As Any, ByVal hpvSource As Long, ByVal cbCopy As Long)
Private Declare Function GetProcessHeap Lib "kernel32" () As Long
Private Declare Function HeapAlloc Lib "kernel32" _
    (ByVal hHeap As Long, ByVal dwFlags As Long, _
    ByVal dwBytes As Long) As Long
Private Declare Function HeapFree Lib "kernel32" (ByVal hHeap As Long, _
    ByVal dwFlags As Long, lpMem As Any) As Long
Private Const MAX_IDE_DRIVES As Long = 4                                        ' Max number of drives assuming primary/secondary, master/slave topology
Private Const READ_ATTRIBUTE_BUFFER_SIZE As Long = 512
Private Const IDENTIFY_BUFFER_SIZE As Long = 512
Private Const READ_THRESHOLD_BUFFER_SIZE As Long = 512
Private Const DFP_GET_VERSION As Long = &H74080
Private Const DFP_SEND_DRIVE_COMMAND As Long = &H7C084
Private Const DFP_RECEIVE_DRIVE_DATA As Long = &H7C088

Private Type GETVERSIONOUTPARAMS
    bVersion As Byte                                                            ' Binary driver version.
    bRevision As Byte                                                           ' Binary driver revision.
    bReserved As Byte                                                           ' Not used.
    bIDEDeviceMap As Byte                                                       ' Bit map of IDE devices.
    fCapabilities As Long                                                       ' Bit mask of driver capabilities.
    dwReserved(3) As Long                                                       ' For future use.
End Type
Private Const CAP_IDE_ID_FUNCTION As Long = 1                                   ' ATA ID command supported
Private Const CAP_IDE_ATAPI_ID As Long = 2                                      ' ATAPI ID command supported
Private Const CAP_IDE_EXECUTE_SMART_FUNCTION As Long = 4                        ' SMART commannds supported
Private Type IDEREGS
    bFeaturesReg As Byte                                                        ' Used for specifying SMART "commands".
    bSectorCountReg As Byte                                                     ' IDE sector count register
    bSectorNumberReg As Byte                                                    ' IDE sector number register
    bCylLowReg As Byte                                                          ' IDE low order cylinder value
    bCylHighReg As Byte                                                         ' IDE high order cylinder value
    bDriveHeadReg As Byte                                                       ' IDE drive/head register
    bCommandReg As Byte                                                         ' Actual IDE command.
    bReserved As Byte                                                           ' reserved for future use. Must be zero.
End Type
Private Type SENDCMDINPARAMS
    cBufferSize As Long                                                         ' Buffer size in bytes
    irDriveRegs As IDEREGS                                                      ' Structure with drive register values.
    bDriveNumber As Byte                                                        ' Physical drive number to send
    ' command to (0,1,2,3).
    bReserved(2) As Byte                                                        ' Reserved for future expansion.
    dwReserved(3) As Long                                                       ' For future use.
    bBuffer(0) As Byte                                                          ' Input buffer.
End Type
Private Const IDE_ATAPI_ID As Long = &HA1                                       ' Returns ID sector for ATAPI.
Private Const IDE_ID_FUNCTION As Long = &HEC                                    ' Returns ID sector for ATA.
Private Const IDE_EXECUTE_SMART_FUNCTION As Long = &HB0                         ' Performs SMART cmd.
Private Const SMART_CYL_LOW As Long = &H4F
Private Const SMART_CYL_HI As Long = &HC2
Private Type DRIVERSTATUS
    bDriverError As Byte                                                        ' Error code from driver,
    bIDEStatus As Byte                                                          ' Contents of IDE Error register.
    bReserved(1) As Byte                                                        ' Reserved for future expansion.
    dwReserved(1) As Long                                                       ' Reserved for future expansion.
End Type
Private Const SMART_NO_ERROR As Long = 0                                        ' No error
Private Const SMART_IDE_ERROR As Long = 1                                       ' Error from IDE controller
Private Const SMART_INVALID_FLAG As Long = 2                                    ' Invalid command flag
Private Const SMART_INVALID_COMMAND As Long = 3                                 ' Invalid command byte
Private Const SMART_INVALID_BUFFER As Long = 4                                  ' Bad buffer (null, invalid addr..)
Private Const SMART_INVALID_DRIVE As Long = 5                                   ' Drive number not valid
Private Const SMART_INVALID_IOCTL As Long = 6                                   ' Invalid IOCTL
Private Const SMART_ERROR_NO_MEM As Long = 7                                    ' Could not lock user's buffer
Private Const SMART_INVALID_REGISTER As Long = 8                                ' Some IDE Register not valid
Private Const SMART_NOT_SUPPORTED As Long = 9                                   ' Invalid cmd flag set
Private Const SMART_NO_IDE_DEVICE As Long = 10                                  ' Cmd issued to device not present
Private Type SENDCMDOUTPARAMS
    cBufferSize As Long                                                         ' Size of bBuffer in bytes
    drvStatus As DRIVERSTATUS                                                   ' Driver status structure.
    bBuffer(0) As Byte                                                          ' Buffer of arbitrary length in which to store the data read from the ' drive.
End Type
Private Const SMART_READ_ATTRIBUTE_VALUES As Long = &HD0                        ' ATA4: Renamed
Private Const SMART_READ_ATTRIBUTE_THRESHOLDS As Long = &HD1                    ' Obsoleted in ATA4!
Private Const SMART_ENABLE_DISABLE_ATTRIBUTE_AUTOSAVE As Long = &HD2
Private Const SMART_SAVE_ATTRIBUTE_VALUES As Long = &HD3
Private Const SMART_EXECUTE_OFFLINE_IMMEDIATE As Long = &HD4                    ' ATA4
Private Const SMART_ENABLE_SMART_OPERATIONS As Long = &HD8
Private Const SMART_DISABLE_SMART_OPERATIONS As Long = &HD9
Private Const SMART_RETURN_SMART_STATUS As Long = &HDA
Private Type DRIVEATTRIBUTE
    bAttrID As Byte                                                             ' Identifies which attribute
    wStatusFlags As Integer                                                     ' see bit definitions below
    bAttrValue As Byte                                                          ' Current normalized value
    bWorstValue As Byte                                                         ' How bad has it ever been?
    bRawValue(5) As Byte                                                        ' Un-normalized value
    bReserved As Byte                                                           ' ...
End Type
Private Type ATTRTHRESHOLD
    bAttrID As Byte                                                             ' Identifies which attribute
    bWarrantyThreshold As Byte                                                  ' Triggering value
    bReserved(9) As Byte                                                        ' ...
End Type
Private Type IDSECTOR
    wGenConfig As Integer
    wNumCyls As Integer
    wReserved As Integer
    wNumHeads As Integer
    wBytesPerTrack As Integer
    wBytesPerSector As Integer
    wSectorsPerTrack As Integer
    wVendorUnique(2) As Integer
    sSerialNumber(19) As Byte
    wBufferType As Integer
    wBufferSize As Integer
    wECCSize As Integer
    sFirmwareRev(7) As Byte
    sModelNumber(39) As Byte
    wMoreVendorUnique As Integer
    wDoubleWordIO As Integer
    wCapabilities As Integer
    wReserved1 As Integer
    wPIOTiming As Integer
    wDMATiming As Integer
    wBS As Integer
    wNumCurrentCyls As Integer
    wNumCurrentHeads As Integer
    wNumCurrentSectorsPerTrack As Integer
    ulCurrentSectorCapacity(3) As Byte                                          '这里只能用byte，因为VB没有无符号的LONG型变量
    wMultSectorStuff As Integer
    ulTotalAddressableSectors(3) As Byte                                        '这里只能用byte，因为VB没有无符号的LONG型变量
    wSingleWordDMA As Integer
    wMultiWordDMA As Integer
    bReserved(127) As Byte
End Type
Private Const ATTR_INVALID As Long = 0
Private Const ATTR_READ_ERROR_RATE As Long = 1
Private Const ATTR_THROUGHPUT_PERF As Long = 2
Private Const ATTR_SPIN_UP_TIME As Long = 3
Private Const ATTR_START_STOP_COUNT As Long = 4
Private Const ATTR_REALLOC_SECTOR_COUNT As Long = 5
Private Const ATTR_READ_CHANNEL_MARGIN As Long = 6
Private Const ATTR_SEEK_ERROR_RATE As Long = 7
Private Const ATTR_SEEK_TIME_PERF As Long = 8
Private Const ATTR_POWER_ON_HRS_COUNT As Long = 9
Private Const ATTR_SPIN_RETRY_COUNT As Long = 10
Private Const ATTR_CALIBRATION_RETRY_COUNT As Long = 11
Private Const ATTR_POWER_CYCLE_COUNT As Long = 12
Private Const PRE_FAILURE_WARRANTY As Long = &H1
Private Const ON_LINE_COLLECTION As Long = &H2
Private Const PERFORMANCE_ATTRIBUTE As Long = &H4
Private Const ERROR_RATE_ATTRIBUTE As Long = &H8
Private Const EVENT_COUNT_ATTRIBUTE As Long = &H10
Private Const SELF_PRESERVING_ATTRIBUTE As Long = &H20
Private Const NUM_ATTRIBUTE_STRUCTS As Long = 30
Private Const INVALID_HANDLE_VALUE As Long = -1
Private Const VER_PLATFORM_WIN32s As Long = 0
Private Const VER_PLATFORM_WIN32_WINDOWS As Long = 1
Private Const VER_PLATFORM_WIN32_NT As Long = 2
Private Type OSVERSIONINFO
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion As String * 128                                                ' Maintenance string for PSS usage
End Type
Private Const CREATE_NEW As Long = 1
Private Const GENERIC_READ As Long = &H80000000
Private Const GENERIC_WRITE As Long = &H40000000
Private Const FILE_SHARE_READ As Long = &H1
Private Const FILE_SHARE_WRITE As Long = &H2
Private Const OPEN_EXISTING As Long = 3
Private m_DiskInfo As IDSECTOR
Private Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFO) As Long
Private Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, ByVal lpSecurityAttributes As Long, ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) As Long
Private Declare Function DeviceIoControl Lib "kernel32" (ByVal hDevice As Long, ByVal dwIoControlCode As Long, lpInBuffer As Any, ByVal nInBufferSize As Long, lpOutBuffer As Any, ByVal nOutBufferSize As Long, lpBytesReturned As Long, ByVal lpOverlapped As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long

Private Declare Function GetVolumeInformation Lib "kernel32" Alias "GetVolumeInformationA" (ByVal lpRootPathName As String, ByVal lpVolumeNameBuffer As String, ByVal nVolumeNameSize As Long, lpVolumeSerialNumber As Long, lpMaximumComponentLength As Long, lpFileSystemFlags As Long, ByVal lpFileSystemNameBuffer As String, ByVal nFileSystemNameSize As Long) As Long
'信息类型枚举
Enum eumInfoType
    hdmodelsn = 0
    hdOnlyModel = 1
    hdOnlySN = 2
End Enum
'磁盘通道枚举
Enum eumDiskNo
    hdPrimaryMaster = 0
    hdPrimarySlave = 1
    hdSecondaryMaster = 2
    hdSecondarySlave = 3
End Enum
'取得逻辑盘序列号 (非唯一)
Function GetDiskVolume(Optional ByVal strDiskName = "C") As String
    Dim TempStr1 As String * 256, TempStr2 As String * 256
    Dim TempLon1 As Long, TempLon2 As Long, GetVal As Long
    
    Dim tmpVol As String
    
    Call GetVolumeInformation(strDiskName & ":\", TempStr1, 256, GetVal, TempLon1, TempLon2, TempStr2, 256)
    If GetVal = 0 Then
        tmpVol = ""
    Else
        tmpVol = Hex(GetVal)
        tmpVol = String(8 - Len(tmpVol), "0") & tmpVol
        tmpVol = Left(tmpVol, 4) & "-" & Right(tmpVol, 4)
    End If
    GetDiskVolume = tmpVol
End Function

Private Function GetMac()
    Dim myNcb As NCB
    Dim bRet As Byte
    myNcb.ncb_command = NCBRESET
    bRet = Netbios(myNcb)
    myNcb.ncb_command = NCBASTAT
    myNcb.ncb_lana_num = 0
    myNcb.ncb_callname = "*　　　　　　 "
    Dim myASTAT As ASTAT, tempASTAT As ASTAT
    Dim pASTAT As Long
    myNcb.ncb_length = Len(myASTAT)
    Debug.Print Err.LastDllError
    pASTAT = HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS _
    Or HEAP_ZERO_MEMORY, myNcb.ncb_length)
    If pASTAT = 0 Then
        Debug.Print "memory allcoation failed!"
        Exit Function
    End If
    myNcb.ncb_buffer = pASTAT
    bRet = Netbios(myNcb)
    Debug.Print Err.LastDllError
    CopyMemory myASTAT, myNcb.ncb_buffer, Len(myASTAT)
    Dim cnt, tmp
    For cnt = 0 To UBound(myASTAT.adapt.adapter_address) - 1
        If Len(Hex(myASTAT.adapt.adapter_address(cnt))) = 1 Then
            tmp = tmp & "0" & Hex(myASTAT.adapt.adapter_address(cnt)) & "-"
        Else
            tmp = tmp & Hex(myASTAT.adapt.adapter_address(cnt)) & "-"
        End If
    Next
    GetMac = Left$(tmp, Len(tmp) - 1)
    HeapFree GetProcessHeap(), 0, pASTAT
End Function


Private Function GetCPUID() As String
    Dim CPUID, strComputer As String
    Dim objWMIService, colDevices, objDevice
    strComputer = "."
    On Error Resume Next
    Set objWMIService = GetObject("winmgmts:\\" & strComputer)
    Set colDevices = objWMIService.ExecQuery _
    ("Select * From Win32_Processor")
    For Each objDevice In colDevices
        GetCPUID = objDevice.ProcessorID
    Next
    
End Function


Public Function GetRegisterCode(Optional RegCodeType As RegCodeTypeEnum = RegCodeType_9_CpuID_MacNumber_DiskID, Optional RegCodeLen As RegCodeLong = RegCodeLong_Len_8, Optional iRegConst As Long = 1) As String
    Dim iString As String
    Dim CPUID As String
    Dim mac As String
    Dim disk As String
    
    CPUID = GetCPUID
    mac = GetMac
    disk = GetDiskVolume(Mid(Environ("WinDir"), 1, 1))
    
    Select Case RegCodeType
    Case Is = 1
        iString = disk
    Case Is = 3
        iString = CPUID
    Case Is = 5
        iString = mac
    Case Is = 4
        iString = disk & "|" & CPUID
    Case Is = 6
        iString = disk & "|" & mac
    Case Is = 8
        iString = CPUID & "|" & mac
    Case Is = 9
        iString = disk & "|" & CPUID & "|" & mac
    End Select
    Debug.Print iString
    
    GetRegisterCode = l.HashString(iString, RegCodeLen, iRegConst)
    
    
    
    
End Function

