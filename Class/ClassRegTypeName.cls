VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "RegisterTypeName"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private iRegN As New RegisterNav

Public Function RegClass(ClassType As String, SubClassType As String) As Boolean
    RegClass = iRegN.CreateKey(HKEY_CLASSES_ROOT, ClassType & "." & SubClassType)
End Function


Public Function RegAutoClass(ClassType As String) As Boolean
    RegAutoClass = iRegN.CreateKey(HKEY_CLASSES_ROOT, ClassType)
End Function

Public Function CheckClass(ClassType As String, SubClassType As String) As Boolean
    CheckClass = iRegN.ItemExits(HKEY_CLASSES_ROOT, ClassType & "." & SubClassType)
End Function

Public Function SetIcon(ClassType As String, SubClassType As String, ICON As String) As Boolean
    SetIcon = iRegN.SetString(HKEY_CLASSES_ROOT, ClassType & "." & SubClassType & "\DefaultIcon", "", ICON)
End Function

Public Function GetIconSet(ClassType As String, SubClassType As String) As String
    GetIconSet = iRegN.GetString(HKEY_CLASSES_ROOT, ClassType & "." & SubClassType & "\DefaultIcon", "")
End Function

Public Function SetClassOpen(ClassType As String, SubClassType As String, OpenCommand As String) As Boolean
    SetClassOpen = iRegN.SetString(HKEY_CLASSES_ROOT, ClassType & "." & SubClassType & "\Shell\Open\Command", "", """" & OpenCommand & """ ""%1""")
End Function
Public Function SetClassOpen2(ClassType As String, OpenCommand As String) As Boolean
    SetClassOpen2 = iRegN.SetString(HKEY_CLASSES_ROOT, ClassType & "\Shell\Open\Command", "", OpenCommand)
End Function
Public Function GetClassOpen(ClassType As String, SubClassType As String) As String
    GetClassOpen = iRegN.GetString(HKEY_CLASSES_ROOT, ClassType & "." & SubClassType & "\Shell\Open\Command", "")
End Function
Public Function GetClassOpen2(Class As String) As String
    GetClassOpen2 = iRegN.GetString(HKEY_CLASSES_ROOT, Class & "\Shell\Open\Command", "")
End Function
Public Function NewAutoMenu(ClassType As String, MenuName As String, OpenCommand As String) As Boolean
    NewAutoMenu = iRegN.SetString(HKEY_CLASSES_ROOT, ClassType & "\Shell\" & MenuName & "\Command", "", OpenCommand & " ""%1""")
End Function

Public Function NewMenu(ClassType As String, SubClassType As String, MenuName As String, OpenCommand As String) As Boolean
    NewMenu = iRegN.SetString(HKEY_CLASSES_ROOT, ClassType & "." & SubClassType & "\Shell\" & MenuName & "\Command", "", OpenCommand & " ""%1""")
End Function

Public Function GetMenu(ClassType As String, SubClassType As String, MenuName As String) As String
    GetMenu = iRegN.GetString(HKEY_CLASSES_ROOT, ClassType & "." & SubClassType & "\Shell\" & MenuName & "\Command", "")
End Function

Public Function NewMenuSub(ClassType As String, SubClassType As String, MenuName As String, SubMenu As String, OpenCommand As String) As Boolean
    NewMenuSub = iRegN.SetString(HKEY_CLASSES_ROOT, ClassType & "." & SubClassType & "\Shell\" & MenuName & "\" & SubMenu & "\Command", "", OpenCommand & " ""%1""")
End Function

Public Function GetMenuSub(ClassType As String, SubClassType As String, MenuName As String, SubMenu As String) As String
    GetMenuSub = iRegN.GetString(HKEY_CLASSES_ROOT, ClassType & "." & SubClassType & "\Shell\" & MenuName & "\" & SubMenu & "\Command", "")
End Function

Public Function DelClass(ClassType As String, SubClassType As String) As Boolean
    DelClass = iRegN.DelKey(HKEY_CLASSES_ROOT, ClassType & "." & SubClassType)
End Function



Public Function GetRegType(RegType, Optional StrReturn As String) As String
    If iRegN.ItemExits(HKEY_CLASSES_ROOT, "." & RegType) <> True Then
        If iRegN.CreateKey(HKEY_CLASSES_ROOT, "." & RegType) = False Then
            Exit Function
        End If
    End If
    GetRegType = iRegN.GetString(HKEY_CLASSES_ROOT, "." & RegType, "")
    StrReturn = GetRegType
End Function


Public Function RegTypeWithAutoClass(ClassType As String, RegType As String) As Boolean
    If iRegN.ItemExits(HKEY_CLASSES_ROOT, ClassType) = True Then
        If iRegN.SetString(HKEY_CLASSES_ROOT, "." & RegType, "", ClassType) = True Then
            RegTypeWithAutoClass = True
        Else
            RegTypeWithAutoClass = False
        End If
    Else
        RegTypeWithAutoClass = False
    End If
    
End Function

Public Function RegTypeWithClass(ClassType As String, SubClassType As String, RegType As String) As Boolean
    If iRegN.ItemExits(HKEY_CLASSES_ROOT, ClassType & "." & SubClassType) = True Then
        If iRegN.SetString(HKEY_CLASSES_ROOT, "." & RegType, "", ClassType & "." & SubClassType) = True Then
            RegTypeWithClass = True
        Else
            RegTypeWithClass = False
        End If
    Else
        RegTypeWithClass = False
    End If
    
End Function

Public Function CheckWebClass(WebClassName As String) As Boolean
    CheckWebClass = iRegN.ItemExits(HKEY_CLASSES_ROOT, ClassType & "." & WebClassName)
End Function

Public Function RegWebClass(WebClassName As String, OpenCommand As String) As Boolean
    RegWebClass = iRegN.CreateKey(HKEY_CLASSES_ROOT, WebClassName)
    RegWebClass = iRegN.SetString(HKEY_CLASSES_ROOT, WebClassName, "", "URL:" & WebClassName & " protocol")
    RegWebClass = iRegN.SetString(HKEY_CLASSES_ROOT, WebClassName, "URL Protocol", "")
    RegWebClass = iRegN.SetString(HKEY_CLASSES_ROOT, WebClassName & "\Shell\Open\Command", "", OpenCommand)
    
End Function

Public Function GetWebClass() As String
    GetWebClass = iRegN.GetString(HKEY_CLASSES_ROOT, WebClassName & "\Shell\Open\Command", "")
    
End Function
