VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Class_Main"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private mFunc As New SomeUsefulFunction
'定义窗体类(事件输出)
Private WithEvents CForm As Class_Form
Attribute CForm.VB_VarHelpID = -1
'定义按钮类
Private WithEvents CButton1 As Class_Button
Attribute CButton1.VB_VarHelpID = -1
Private WithEvents CButton2 As Class_Button
Attribute CButton2.VB_VarHelpID = -1
Private WithEvents CButton3 As Class_Button
Attribute CButton3.VB_VarHelpID = -1
'定义菜单类
Private WithEvents CMenus As Class_MenuManage
Attribute CMenus.VB_VarHelpID = -1
Private Declare Function GetTickCount Lib "kernel32" () As Long

'点击按钮
Private Sub CButton1_Click()
    mFunc.MsgBox_A "now is:" & mFunc.GetNowTime, , CForm.hWnd
End Sub

Private Sub CButton2_Click()
    Dim iStart As Long
    Dim i As Long
    iStart = GetTickCount
    For i = 1 To 20000
        CButton1.Caption = i
    Next
    mFunc.MsgBox_A GetTickCount - iStart, "耗时", CForm.hWnd, Information
End Sub

Private Sub CButton3_Click()
    mFunc.MsgBox_A CApp.CommandLine, "command line parameter", CForm.hWnd
End Sub

'主类构造函数
Private Sub Class_Initialize()
    '初始化系统对象
    Call sysInitialize
    '类实例化
    Set CForm = New Class_Form
    '设置参数
    With CForm
        .width = 400
        .height = 300
        .Center = True
        .Caption = "你好!"
    End With
    '托管窗体类
    If Trusteeship(CForm) = False Then Debug.Print CApp.ErrDescription
End Sub

'主类析构函数
Private Sub Class_Terminate()
    '释放类
    Set CForm = Nothing
End Sub

'---------------------------------------------------------------------------------------------------------------
'窗体类事件
'---------------------------------------------------------------------------------------------------------------

Public Sub CForm_Create()
    '创建窗体时添加控件
    
    '按钮
    Set CButton1 = New Class_Button
    CButton1.AppendButton CForm, 10, 10, 100, 30
    CButton1.Caption = "测试按钮1"
    CButton1.Visible = True
    
    Set CButton2 = New Class_Button
    CButton2.AppendButton CForm, 10, 50, 100, 30
    CButton2.Caption = "测试按钮2"
    CButton2.Visible = True
    
    Set CButton3 = New Class_Button
    CButton3.AppendButton CForm, 10, 90, 100, 30
    CButton3.Caption = "测试按钮3"
    CButton3.Visible = True
    
    '菜单
    Set CMenus = New Class_MenuManage
    With CMenus
        If .Initial(CForm.hWnd) = False Then End
        
        .AddForm("文件(&S)", "File").HaveSub = True
        .AddStd "File", "新建(&N)         Ctrl+N", "New"
        .AddStd "File", "打开(&O)...   Ctrl+O"
        .AddStd "File", "保存(&S)        Ctrl+S"
        .AddStd("File", "另存为(&A)...", "SaveAs").HaveSub = True
        .AddStd "SaveAs", "另存为 网页(&H)"
        .AddStd "SaveAs", "另存为 文本(&T)"
        .AddStd("File").IsLine = True
        .AddStd "File", "页面设置(&U)..."
        .AddStd "File", "打印(&P)...     Ctrl+P"
        .AddStd("File").IsLine = True
        .AddStd "File", "退出(&X)", "Exit"
        
        .AddForm("编辑(&E)", "Edit").HaveSub = True
        .AddStd("Edit", "撤销(&U)      Ctrl+Z").Enabled = False
        
        .AddStd("Edit").IsLine = True
        
        .AddStd "Edit", "剪切(&T)       Ctrl+X"
        .AddStd "Edit", "复制(&C)      Ctrl+C"
        .AddStd "Edit", "Paste(&P)     Ctrl+V"
        .AddStd "Edit", "Delete(&D)"
        
        .AddStd("Edit").IsLine = True
        
        .AddStd "Edit", "Find(&F)Ctrl+F"
        .AddStd "Edit", "Find Nex(&N)F3"
        .AddStd "Edit", "Replace(&R)Ctrl+H"
        .AddStd "Edit", "To Row(&G)Ctrl+G"
        
        .AddStd("Edit").IsLine = True
        
        .AddStd "Edit", "Select All(&A)Ctrl+A"
        .AddStd "Edit", "Time/Date(&D) F5"
        
        .AddForm("格式(&F)", "Format").HaveSub = True
        .AddStd("Format", "Auto Wrap(&W)", "Wrap").Checked = True
        .AddStd "Format", "Font(&F)..."
        
        .AddForm("窗口(&W)", "Window").HaveSub = True
        .AddStd("Window", "Split(&S)", "Split").Checked = True
        
        .AddStd("Window").IsLine = True
        With .AddStd("Window", "Horizontal Tile(&H)", "HTile")
            .IsRadio = True
            .Checked = True
        End With
        .AddStd("Window", "Verticality Tile(&V)", "VTile").IsRadio = True
        .AddStd("Window", "Cascade(&C)", "Cascade").IsRadio = True
        .AddStd("Window", "Icon(&I)", "Icon").IsRadio = True
        
        .AddStd("Window").IsLine = True
        
        .AddStd("Window", "Window Lists(&L)", "WindowList").HaveSub = True
        .AddStd "WindowList", "[" & CForm.Caption & "]"
        
        With .AddForm("帮助(&H)", "Help")
            .HaveSub = True
            .IsHelp = True
        End With
        .AddStd "Help", "Theme(&T)", "Theme"
        .AddStd("Help").IsLine = True
        .AddStd "Help", "About(&A)", "About"
        
        .AddPop "UnDo(&U)"
        .AddPop.IsLine = True
        .AddPop "Cut(&T)"
        .AddPop "Copy(&C)"
        .AddPop "Paste(&P)"
        .AddPop "Delete(&D)"
        .AddPop.IsLine = True
        .AddPop "Select All(&A)"
        .AddPop.IsLine = True
        
        .AddPop("Language(&L)", "Language").HaveSub = True
        .AddStd "Language", "Chinese(GB2312)(&C)"
        .AddStd "Language", "English(UTF-8)(&E)"
        
        .AddSys.IsLine = True
        .AddSys "About(&A)...", "About"
        .AddSys.IsLine = True
        With .GetItemByAlias("Window")
            '将Window菜单绑定到该项
            CMenus.AddSys(.Caption, , .hItem).HaveSub = True
        End With
        .AddSys("TopMost(&T)", "TopMost").Checked = True
        
        .Enabled = True
        
        '按下新建菜单
        .EventMethod .GetItemByAlias("New")
    End With
    
    
    
    '窗体置顶
    CForm.MostTop = True
End Sub

'菜单事件
Private Sub CMenus_Click(MenuItem As Class_MenuItem)
    With MenuItem
        Select Case .Alias
        Case "TopMost"
            If CForm.MostTop = True Then
                CForm.MostTop = False
            Else
                CForm.MostTop = True
            End If
        Case "New"
            CForm.Caption = "新建"
        Case "Exit"
            '卸载窗体
            CApp.Unload CForm
        Case "HTile", "VTile", "Cascade", "Icon"
            With CMenus
                .GetItemByAlias("HTile").Checked = False
                .GetItemByAlias("VTile").Checked = False
                .GetItemByAlias("Cascade").Checked = False
                .GetItemByAlias("Icon").Checked = False
            End With
            .Checked = True
        Case "About"
            mFunc.MsgBox_A "构建窗体", , CForm.hWnd
        Case "Wrap", "Split", "TopMost"
            .Checked = Not .Checked
        End Select
    End With
End Sub

Private Sub CForm_MouseMove(ByVal X As Integer, ByVal Y As Integer)
    CForm.Caption = CStr(X) & "/" & CStr(Y)
End Sub

Private Sub CForm_Unload(Cancel As Boolean)
    If mFunc.MsgBox_A("确定么", "退出程序:", CForm.hWnd, YesNo) = No Then
        Cancel = True
    Else
        mFunc.MsgBox_A "再见!", , CForm.hWnd
    End If
End Sub


