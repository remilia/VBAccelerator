VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "InlineASM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private asm As New ASMBler
Private asmCode As Memory
Public Enum retType
    NoRet
    Str
    Lng
    Dbl
    Sng
    byteArr
    Byt
End Enum
Public Sub Assemble(ByVal strASMCode As String)
    asmCode = AsmToMem(strASMCode)
End Sub

Public Sub FreeASM()
    FreeMemory asmCode
End Sub

Public Function CallFunction(Optional ByVal arg1 = 0&, Optional ByVal arg2 = 0&, Optional ByVal arg3 = 0&, Optional retValueType As retType, Optional ByVal byteArrLenght As Long = 0, Optional ret As Variant) As Long
    Dim strRet As String
    Dim lngRet As Long
    Dim dblRet As Double
    Dim sngRet As Single
    Dim byteArr() As Byte
    Dim bytRet As Byte
    Select Case retValueType
    Case Is = retType.NoRet
        CallFunction = CallWindowProc(ByVal asmCode.address, 0&, 0&, 0&, 0&)
    Case Is = retType.Byt
        CallFunction = CallWindowProc(ByVal asmCode.address, arg1, arg2, arg3, bytRet)
        ret = bytRet
    Case Is = retType.byteArr
        ReDim byteArr(byteArrLenght)
        CallFunction = CallWindowProc(ByVal asmCode.address, arg1, arg2, arg3, byteArr(0))
        ret = byteArr
    Case Is = retType.Dbl
        CallFunction = CallWindowProc(ByVal asmCode.address, arg1, arg2, arg3, dblRet)
        ret = dblRet
    Case Is = retType.Sng
        CallFunction = CallWindowProc(ByVal asmCode.address, arg1, arg2, arg3, sngRet)
        ret = sngRet
    Case Is = retType.Str
        CallFunction = CallWindowProc(ByVal asmCode.address, arg1, arg2, arg3, strRet)
        ret = strRet
    Case Else
        CallFunction = CallWindowProc(ByVal asmCode.address, arg1, arg2, arg3, lngRet)
        ret = lngRet
    End Select
End Function

Private Sub Class_Terminate()
    FreeMemory asmCode
End Sub
