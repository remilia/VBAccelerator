VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DMD5Hash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum HashEnum
    lpHash16Byte = 16
    lpHash32Byte = 32
    lpHash64Byte = 64
    lpHash128Byte = 128
End Enum

Public Function HashString(str As String, HashType As HashEnum, Optional Count As Long = 1) As String
    Initialize
    Dim xNumber As Long
    Dim xStr As String
    xNumber = CLng(Val(Count))
    xStr = CStr(str)
    Dim xA As Long
    Dim xB As Long
    Dim xC As Long
    Dim xD As Long
    Dim i As Long
    For i = 1 To Count
        MD5 xStr, xA, xB, xC, xD
        If HashType = 16 Then
            HashString = LCase(WordToHex(xD) & WordToHex(xC))
        ElseIf HashType = 32 Then
            HashString = LCase(WordToHex(xB) & WordToHex(xC) & WordToHex(xA) & WordToHex(xD))
        ElseIf HashType = 64 Then
            HashString = LCase(WordToHex(xA) & WordToHex(xB) & WordToHex(xC) & WordToHex(xD) & WordToHex(xC) & WordToHex(xB) & WordToHex(xD) & WordToHex(xA))
        ElseIf HashType = 128 Then
            HashString = LCase(WordToHex(xA) & WordToHex(xB) & WordToHex(xC) & WordToHex(xD) & WordToHex(xC) & WordToHex(xB) & WordToHex(xD) & WordToHex(xA) & WordToHex(xA) & WordToHex(xB) & WordToHex(xC) & WordToHex(xD) & WordToHex(xA) & WordToHex(xC) & WordToHex(xD) & WordToHex(xB))
        End If
        xStr = HashString
    Next
End Function

Public Function Hash(byt() As Byte, HashType As HashEnum, Optional Count As Long = 1) As String
    Initialize
    Dim xNumber As Long
    Dim xStr As String
    Initialize
    xNumber = CLng(Val(Count))
    xStr = StrConv(byt, vbUnicode)
    Dim xA As Long
    Dim xB As Long
    Dim xC As Long
    Dim xD As Long
    Dim i As Long
    For i = 1 To Count
        MD5 xStr, xA, xB, xC, xD
        If HashType = 16 Then
            Hash = LCase(Hex(xA))
            DoEvents
        ElseIf HashType = 32 Then
            Hash = LCase(Hex(xC) & Hex(xD))
            DoEvents
        ElseIf HashType = 64 Then
            Hash = LCase(Hex(xB) & Hex(xC) & Hex(xD))
            DoEvents
        ElseIf HashType = 128 Then
            Hash = LCase(Hex(xA) & Hex(xB) & Hex(xC) & Hex(xD))
            DoEvents
        End If
        xStr = Hash
    Next
End Function

Public Function HashNumber(pStr As String, HashType As HashEnum, Optional Count As Long = 1) As Long
    Initialize
    Dim xNumber As Long
    Dim xStr As String
    Initialize
    xNumber = CLng(Val(Count))
    xStr = pStr
    Dim xA As Long
    Dim xB As Long
    Dim xC As Long
    Dim xD As Long
    Dim i As Long
    For i = 1 To Count
        MD5 xStr, xA, xB, xC, xD
        If HashType = 16 Then
            HashNumber = "&H" & LCase(Hex(xA))
            DoEvents
        ElseIf HashType = 32 Then
            HashNumber = "&H" & LCase(Hex(xC) & Hex(xD))
            DoEvents
        ElseIf HashType = 64 Then
            HashNumber = "&H" & LCase(Hex(xB) & Hex(xC) & Hex(xD))
            DoEvents
        ElseIf HashType = 128 Then
            HashNumber = "&H" & LCase(Hex(xA) & Hex(xB) & Hex(xC) & Hex(xD))
            DoEvents
        End If
        xStr = HashNumber
    Next
End Function

Private Sub Class_Initialize()
    Initialize
End Sub
