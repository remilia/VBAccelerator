VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "INIFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Function WritePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileSectionA" ( _
 _
    ByVal lpAppName As String, _
    ByVal lpString As String, _
    ByVal lpFileName As String) As Long
                        
                        
Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" ( _
 _
    ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, _
    ByVal lpString As Any, _
    ByVal lpFileName As String) As Long
        
        
    'Public Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" ( _
 _
    ByVal lpApplicationName As String, _
    ByVal lpKeyName As String, _
    ByVal nDefault As Long, _
    ByVal lpFileName As String) As Long
                      

    'Public Declare Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" ( _
 _
    ByVal lpAppName As String, _
    ByVal lpReturnedString As String, _
    ByVal nSize As Long, _
    ByVal lpFileName As String) As Long
        
        
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" ( _
 _
    ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, _
    ByVal lpDefault As String, _
    ByVal lpReturnedString As String, _
    ByVal nSize As Long, _
    ByVal lpFileName As String) As Long

Public Function INI_WriteString(FilePath As String, Section, KeyName, KeyString)
    WritePrivateProfileString CStr(Section), CStr(KeyName), CStr(KeyString), FilePath
    
End Function


Public Function INI_GetString(FilePath As String, Section As String, KeyName As String) As String
    
    Dim xSize As Long, kTemp As String, s As Integer
    xSize = 255
    kTemp = String(xSize, 0)
    GetPrivateProfileString Section, KeyName, "", kTemp, xSize, FilePath
    s = InStr(kTemp, Chr(0))
    If s > 0 Then
        kTemp = Left(kTemp, s - 1)
        INI_GetString = kTemp
    End If
End Function



