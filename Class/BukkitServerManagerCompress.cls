VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ZIPByteCompress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (hpvDest As Any, hpvSource As Any, ByVal cbCopy As Long)
Private Declare Function Compress Lib "zlib.dll" Alias "compress" (Dest As Any, destLen As Any, Src As Any, ByVal srcLen As Long) As Long
Private Declare Function UnCompress Lib "zlib.dll" Alias "Uncompress" (Dest As Any, destLen As Any, Src As Any, ByVal srcLen As Long) As Long

Private Const OFFSET As Long = &H8

'压缩
Public Function CompressByte(ByteArray() As Byte)
    Dim BufferSize As Long
    Dim TempBuffer() As Byte
    '创建一个缓冲区来保存压缩数据
    
    BufferSize = UBound(ByteArray) + 1
    BufferSize = BufferSize + (BufferSize * 0.01) + 12
    ReDim TempBuffer(BufferSize)
    
    '压缩字节数组(数据)
    CompressByte = (Compress(TempBuffer(0), BufferSize, ByteArray(0), UBound(ByteArray) + 1) = 0)
    
    '“添加原始数据的大小
    Call CopyMemory(ByteArray(0), CLng(UBound(ByteArray) + 1), OFFSET)
    
    ''去除冗余数据
    
    ReDim Preserve ByteArray(0 To BufferSize + OFFSET - 1)
    CopyMemory ByteArray(OFFSET), TempBuffer(0), BufferSize
    CompressByte = ByteArray
End Function

'压缩
Public Function UnCompressByte(ByteArray() As Byte) As Byte()
    Dim OrigLen As Long
    Dim BufferSize As Long
    Dim TempBuffer() As Byte
    'Get the original size
    Call CopyMemory(OrigLen, ByteArray(0), OFFSET)
    
    'Create a buffer to hold the uncompressed data
    BufferSize = OrigLen
    BufferSize = BufferSize + (BufferSize * 0.01) + 12
    ReDim TempBuffer(BufferSize)
    
    'Decompress data
    UnCompress TempBuffer(0), BufferSize, ByteArray(OFFSET), UBound(ByteArray) - OFFSET + 1
    
    'Remove redundant data
    ReDim Preserve ByteArray(0 To BufferSize - 1)
    CopyMemory ByteArray(0), TempBuffer(0), BufferSize
    UnCompressByte = ByteArray
End Function
