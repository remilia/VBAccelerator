VERSION 5.00
Begin VB.UserControl GIF 
   AutoRedraw      =   -1  'True
   BackColor       =   &H80000007&
   ClientHeight    =   3090
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3135
   ScaleHeight     =   206
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   209
   ToolboxBitmap   =   "MyGIF.ctx":0000
End
Attribute VB_Name = "GIF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private Declare Function CLSIDFromString Lib "ole32.dll" (ByVal lpszProgID As Long, pCLSID As CLSID) As Long
Private Declare Function GdiplusStartup Lib "gdiplus" (Token As Long, inputbuf As GdiplusStartupInput, Optional ByVal outputbuf As Long = 0) As GpStatus
Private Declare Sub GdiplusShutdown Lib "gdiplus" (ByVal Token As Long)
Private Declare Function GdipCreateFromHDC Lib "gdiplus" (ByVal hDC As Long, graphics As Long) As GpStatus
Private Declare Function GdipLoadImageFromFile Lib "gdiplus" (ByVal filename As String, Image As Long) As GpStatus
Private Declare Function GdipGetImageWidth Lib "gdiplus" (ByVal Image As Long, width As Long) As GpStatus
Private Declare Function GdipGetImageHeight Lib "gdiplus" (ByVal Image As Long, height As Long) As GpStatus
Private Declare Function GdipImageGetFrameCount Lib "gdiplus" (ByVal Image As Long, dimensionID As CLSID, Count As Long) As GpStatus
Private Declare Function GdipGetPropertyItemSize Lib "gdiplus" (ByVal Image As Long, ByVal propId As Long, size As Long) As GpStatus
Private Declare Function GdipGetPropertyItem Lib "gdiplus" (ByVal Image As Long, ByVal propId As Long, ByVal propSize As Long, Buffer As PropertyItem) As GpStatus
Private Declare Function GdipImageSelectActiveFrame Lib "gdiplus" (ByVal Image As Long, dimensionID As CLSID, ByVal FrameIndex As Long) As GpStatus
Private Declare Function GdipDrawImageRectI Lib "gdiplus" (ByVal graphics As Long, ByVal Image As Long, ByVal X As Long, ByVal Y As Long, ByVal width As Long, ByVal height As Long) As GpStatus
Private Declare Function GdipDisposeImage Lib "gdiplus" (ByVal Image As Long) As GpStatus
Private Declare Function GdipDeleteGraphics Lib "gdiplus" (ByVal graphics As Long) As GpStatus
Private Declare Function GdipGetImageEncodersSize Lib "gdiplus" (numEncoders As Long, size As Long) As GpStatus
Private Declare Function GdipGetImageEncoders Lib "gdiplus" (ByVal numEncoders As Long, ByVal size As Long, encoders As Any) As GpStatus
Private Declare Function GdipGetImageDecodersSize Lib "gdiplus" (numDecoders As Long, size As Long) As GpStatus
Private Declare Function GdipGetImageDecoders Lib "gdiplus" (ByVal numDecoders As Long, ByVal size As Long, decoders As Any) As GpStatus
Private Declare Function GetDIBits Lib "gdi32" (ByVal aHDC As Long, ByVal hBitmap As Long, ByVal nStartScan As Long, ByVal nNumScans As Long, lpBits As Any, lpBI As BITMAPINFO, ByVal wUsage As Long) As Long
Private Declare Function lstrlenW Lib "kernel32" (ByVal psString As Any) As Long
Private Declare Function lstrlenA Lib "kernel32" (ByVal psString As Any) As Long
Private Declare Function GetTickCount Lib "kernel32" () As Long
Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

Private Const PropertyTagTypeByte = 1
Private Const PropertyTagTypeUndefined = 7
Private Const PropertyTagTypeASCII = 2
Private Const PropertyTagTypeShort = 3
Private Const PropertyTagTypeLong = 4
Private Const PropertyTagTypeRational = 5
Private Const PropertyTagTypeSLONG = 9
Private Const PropertyTagTypeSRational = 10
Private Const PropertyTagFrameDelay = &H5100
Private Const FrameDimensionTime       As String = "{6AEDBD6D-3FB5-418A-83A6-7F45229DC872}"

Private Type GdiplusStartupInput
    GdiplusVersion As Long
    DebugEventCallback As Long
    SuppressBackgroundThread As Long
    SuppressExternalCodecs As Long
End Type

Private Type CLSID
    Data1 As Long
    Data2 As Integer
    Data3 As Integer
    Data4(0 To 7) As Byte
End Type

Private Type PropertyItem
    propId As Long
    Length As Long
    type As Integer
    Value As Long
End Type

Private Enum GpStatus
    Ok = 0
    GenericError = 1
    InvalidParameter = 2
    OutOfMemory = 3
    ObjectBusy = 4
    InsufficientBuffer = 5
    NotImplemented = 6
    Win32Error = 7
    WrongState = 8
    Aborted = 9
    FileNotFound = 10
    ValueOverflow = 11
    AccessDenied = 12
    UnknownImageFormat = 13
    FontFamilyNotFound = 14
    FontStyleNotFound = 15
    NotTrueTypeFont = 16
    UnsupportedGdiplusVersion = 17
    GdiplusNotInitialized = 18
    PropertyNotFound = 19
    PropertyNotSupported = 20
End Enum
Private Enum ImageCodecFlags
    ImageCodecFlagsEncoder = &H1
    ImageCodecFlagsDecoder = &H2
    ImageCodecFlagsSupportBitmap = &H4
    ImageCodecFlagsSupportVector = &H8
    ImageCodecFlagsSeekableEncode = &H10
    ImageCodecFlagsBlockingDecode = &H20

    ImageCodecFlagsBuiltin = &H10000
    ImageCodecFlagsSystem = &H20000
    ImageCodecFlagsUser = &H40000
End Enum
Private Type ImageCodecInfo
    ClassID As CLSID
    FormatID As CLSID
    CodecName As Long
    DllName As Long
    FormatDescription As Long
    FilenameExtension As Long
    MimeType As Long
    Flags As ImageCodecFlags
    Version As Long
    SigCount As Long
    SigSize As Long
    SigPattern As Long
    SigMask As Long
End Type
Private Type BITMAPINFOHEADER                                                   '40 bytes
    biSize As Long
    biWidth As Long
    biHeight As Long
    biPlanes As Integer
    biBitCount As Integer
    biCompression As Long
    biSizeImage As Long
    biXPelsPerMeter As Long
    biYPelsPerMeter As Long
    biClrUsed As Long
    biClrImportant As Long
End Type
Private Type RGBQUAD
    rgbBlue As Byte
    rgbGreen As Byte
    rgbRed As Byte
    rgbReserved As Byte
End Type
Private Type BITMAPINFO
    bmiHeader As BITMAPINFOHEADER
    bmiColors As RGBQUAD
End Type

Private Token As Long
Private gStartInput As GdiplusStartupInput
Private gStatus As GpStatus
Private gFrameCLSID As CLSID

Private gDraw As Long
'存储容器
Private gImage As Long
'存储图像
'''''''''''''''''''''''''
'GDI+
'''''''''''''''''''''''''
Private bStretch As Boolean
Private bPlayStatus As Boolean
Private lngNowFrame As Long
'''''''''''''''''''''''''
Private gImageProp() As PropertyItem
Private alngDelay() As Long

Public Event PlayingFrameSwitching(NowFrame As Long)
Public Event PlayingEnd()
Public Event PlayingStart()
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Private Function PtrToStrA(ByVal lpsz As Long) As String
    Dim sOut As String
    Dim lLen As Long
    
    lLen = lstrlenA(lpsz)
    
    If (lLen > 0) Then
        sOut = String$(lLen, vbNullChar)
        Call CopyMemory(ByVal sOut, ByVal lpsz, lLen)
        PtrToStrA = sOut
    End If
End Function

Private Function PtrToStrW(ByVal lpsz As Long) As String
    Dim sOut As String
    Dim lLen As Long
    
    lLen = lstrlenW(lpsz)
    
    If (lLen > 0) Then
        sOut = StrConv(String$(lLen, vbNullChar), vbUnicode)
        Call CopyMemory(ByVal sOut, ByVal lpsz, lLen * 2)
        PtrToStrW = StrConv(sOut, vbFromUnicode)
    End If
End Function
Private Function GetPropValue(item As PropertyItem) As Variant
    If item.Value = 0 Or item.Length = 0 Then Err.Raise 5, "GetPropValue"
    
    Select Case item.type
    Case PropertyTagTypeByte, PropertyTagTypeUndefined:
        Dim bte() As Byte: ReDim bte(1 To item.Length)
        CopyMemory bte(1), ByVal item.Value, item.Length
        GetPropValue = bte
        Erase bte
        
    Case PropertyTagTypeASCII:
        GetPropValue = PtrToStrA(item.Value)
        
    Case PropertyTagTypeShort:
        Dim short() As Integer: ReDim short(1 To (item.Length / 2))
        CopyMemory short(1), ByVal item.Value, item.Length
        GetPropValue = short
        Erase short
        
    Case PropertyTagTypeLong, PropertyTagTypeSLONG:
        Dim lng() As Long: ReDim lng(1 To (item.Length / 4))
        CopyMemory lng(1), ByVal item.Value, item.Length
        GetPropValue = lng
        Erase lng
        
    Case PropertyTagTypeRational, PropertyTagTypeSRational:
        Dim lngpair() As Long: ReDim lngpair(1 To (item.Length / 8), 1 To 2)
        CopyMemory lngpair(1, 1), ByVal item.Value, item.Length
        GetPropValue = lngpair
        Erase lngpair
        
    Case Else: Exit Function
    End Select
End Function


'GIF帧数据
Private Sub UserControl_Initialize()
    CLSIDFromString StrPtr(FrameDimensionTime), gFrameCLSID
    
    gStartInput.GdiplusVersion = 1
    gStatus = GdiplusStartup(Token, gStartInput)
    '初始化GDI+
    Call ReturnCodeCheck("GDI+启动时错误")
    UserControl.height = Screen.height
    UserControl.width = Screen.width
    
    gStatus = GdipCreateFromHDC(UserControl.hDC, gDraw)
    '初始化绘图对象
    UserControl.height = 128
    UserControl.width = 128
    Call ReturnCodeCheck("GDI+初始化错误")
    
End Sub

'重新设定控件大小
Private Sub UserControl_Resize()
    '如果缩放打开，且载入了图像，就缩放控件到图像大小
    If bStretch = False Then
        If ImageLoaded = True Then
            UserControl.height = ImageHeight * 15
            UserControl.width = ImageWidth * 15
        End If
    Else
        
    End If
End Sub

Private Sub UserControl_Terminate()
    GdipDisposeImage gImage
    '释放图像
    GdipDeleteGraphics gDraw
    '释放容器
    GdiplusShutdown Token
    '关闭gdi+
    bPlayStatus = False
    '关闭播放
End Sub



'读取一个GIF文件
Public Sub LoadGIF(ByVal strPath As String)
Attribute LoadGIF.VB_Description = "载入GIF文件"
    If ImageLoaded = True Then
        GdipDisposeImage (gImage)
        gImage = 0
    End If
    bPlayStatus = False
    lngNowFrame = 0
    gStatus = GdipLoadImageFromFile(StrConv(strPath, vbUnicode), gImage)
    '使用GdipLoadImageFromFile读取GIF文件
    Call ReturnCodeCheck("图像载入错误")
    '载入图像之后，读取帧数据
    
    Dim lngAllBuffSize As Long
    Dim lngCount As Long
    
    GdipGetPropertyItemSize gImage, PropertyTagFrameDelay, lngAllBuffSize
    '获得属性所占字节大小
    Call ReturnCodeCheck("获取GIF帧数错误")
    
    ReDim gImageProp(0)
    lngCount = lngAllBuffSize / LenB(gImageProp(0))
    ReDim gImageProp(0 To lngCount)
    '申请空间，读入属性
    GdipGetPropertyItem gImage, PropertyTagFrameDelay, lngAllBuffSize, gImageProp(0)
    '获得帧与帧之间延迟的数组
    alngDelay = GetPropValue(gImageProp(0))
    
End Sub

'仅播放一次
Public Sub PlayOnce()
    If ImageLoaded = False Then Exit Sub
    Dim lngDelay As Long
    lngNowFrame = 0
    bPlayStatus = True
    '设置播放中标志
    RaiseEvent PlayingStart
    '触发开始事件
    For lngNowFrame = 0 To FrameCount - 1
        GdipImageSelectActiveFrame gImage, gFrameCLSID, lngNowFrame
        '切换帧
        GdipDrawImageRectI gDraw, gImage, 0, 0, UserControl.ScaleWidth, UserControl.ScaleHeight
        '绘制
        'Label1.Caption = "G:" & gDraw & ",I:" & gImage
        lngDelay = GetTickCount()
        '获得时间，并延迟
        Do While GetTickCount < lngDelay + NextFramesDelay(lngNowFrame)
            If bPlayStatus = False Then Exit Sub
            DoEvents
            Sleep 1
        Loop
        UserControl.Refresh
        RaiseEvent PlayingFrameSwitching(lngNowFrame)
        '触发帧切换事件
        
    Next
    RaiseEvent PlayingEnd
    '触发结束事件
    
End Sub

'循环播放
Public Sub PlayLoop()
    bPlayStatus = True
    Do Until bPlayStatus = False: PlayOnce: DoEvents: Sleep 1: Loop
End Sub

'暂停播放
Public Sub PausePlay()
    bPlayStatus = False
End Sub

'继续播放
Public Sub ResumePlay()
    bPlayStatus = True
End Sub

'停止播放，帧位归0
Public Sub StopPlay()
    bPlayStatus = False
    lngNowFrame = 0
End Sub

'当前帧，属性
Public Property Let NowFrame(Value As Long)
    lngNowFrame = Value
    If ImageLoaded = True Then
        GdipImageSelectActiveFrame gImage, gFrameCLSID, Value
        GdipDrawImageRectI gDraw, gImage, 0, 0, UserControl.ScaleWidth, UserControl.ScaleHeight
        UserControl.Refresh
    End If
End Property

Public Property Get NowFrame() As Long
    NowFrame = lngNowFrame
End Property



'下一帧的延时
Public Property Get NextFramesDelay(FrameIndex As Long) As Long
    '如果是最后一帧
    If FrameIndex = FrameCount Then
        NextFramesDelay = 0
    End If
    NextFramesDelay = alngDelay(FrameIndex + 1) * 10
End Property

'帧数总和
Public Property Get FrameCount() As Long
    '如果没有载入GIF图像，就直接置零
    If ImageLoaded = False Then
        FrameCount = 0
        Exit Property
    End If
    '获取帧数总和
    GdipImageGetFrameCount gImage, gFrameCLSID, FrameCount
End Property


'判断图像是否载入
Public Property Get ImageLoaded() As Boolean
    ImageLoaded = gImage <> 0
End Property

'获得GIF的高
Public Property Get ImageHeight() As Long
    '如果没有载入图像，就不执行代码
    If ImageLoaded = False Then
        ImageHeight = 64
        Exit Property
    End If
    '如果载入了图像，就返回高度
    GdipGetImageHeight gImage, ImageHeight
End Property

'获得GIF的宽
Public Property Get ImageWidth() As Long
    '如果没有载入图像，就不执行代码
    If ImageLoaded = False Then
        ImageWidth = 64
        Exit Property
    End If
    '如果载入了图像，就返回宽度
    GdipGetImageWidth gImage, ImageWidth
End Property

'是否自动缩放
Public Property Let Stretch(Value As Boolean)
    bStretch = Value
    UserControl_Resize
End Property

Public Property Get Stretch() As Boolean
    Stretch = bStretch
End Property

Private Sub ReturnCodeCheck(ByVal ErrorTips As String)
    '判断gStatus返回值是不是OK，如果不是
    Debug.Print gStatus & ".:" & IIf(gStatus = Ok, "No Error ", ErrorTips)
    If gStatus <> Ok Then
        '就报错
        Err.Raise gStatus, ErrorTips & ":" & gStatus
        Exit Sub
    End If
End Sub


















